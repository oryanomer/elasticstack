# ElasticStack

### In This Repo, you will have three different Platform to deploy Elastic Stack at your environment

* The first is deploying Elastic stack on VMS.
The deployment process is done by Ansible playbooks.

* The second is deploying Elastic Stack on Docker Swarm.

* The third is deploying Elastic Stack on Kubernetes.

### Each option has is own description and deployment process in his folder. 

> I will happy to get feedback!.    


![Elastic Image](https://miro.medium.com/max/892/1*AYP0Mg_MwJMm3Kbx8Xa8lQ.png)
