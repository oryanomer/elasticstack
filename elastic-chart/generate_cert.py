"""
This tool aims to help you generate certificates for your own.
here you will be able to generate CA Certificate, Certificate from your CA.
"""
from subprocess import check_call
import sys
import os

def gen_ca():
    check_call(['openssl','genrsa','-out',os.environ("pwd")/ca.key,'2048'])
    check_call(['openssl','rsa','-in','example.org.key' '-noout -text'])